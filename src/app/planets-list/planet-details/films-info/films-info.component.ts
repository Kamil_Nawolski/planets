import { Component, OnInit, Input } from '@angular/core';
import { Film } from 'src/app/models/Film';
import { PlanetsService } from 'src/app/services/planets.service';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material';
import { Planet } from 'src/app/models/Planet';

@Component({
  selector: 'app-films-info',
  templateUrl: './films-info.component.html',
  styleUrls: ['./films-info.component.sass']
})
export class FilmsInfoComponent implements OnInit {

  @Input() planet: Planet;

  displayedColumns: string[] = ['title', 'director', 'producer', 'release_date'];
  dataSource = new MatTableDataSource<Film[]>();
  isLoading: Boolean = false;

  constructor(private _planetsService: PlanetsService) { }

  ngOnInit() {
    this.getFilms(this.planet);
  }

  setUnknownClass(element) {
		return { 'unknown': element === 'unknown' }
	}

	hideUnit(element): boolean {
		return element !== 'unknown';
	}

  getIdFilms(url: string) {
    let id = /\d+/g;
    return id.exec(url);
  }

  getFilms(planet: Planet) {
		this.isLoading = true;
		let residents = planet.films;
		let films = [];
		residents.forEach(element => {
			let ids = this.getIdFilms(element);
			of(ids['0']).pipe(
				mergeMap(x => this._planetsService.getFilmsDetails(x))
			).subscribe(i => {
				films.push(i);
        this.dataSource = new MatTableDataSource<Film[]>(films);
			},
				(err: HttpErrorResponse) => console.log(`nie udało się pobrać danych: błąd ${err.status}`),
				() => this.isLoading = false
			);
		});
	}

}
