import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.sass']
})
export class GeneralInfoComponent implements OnInit {

  @Input() planet;

  constructor() { }

  ngOnInit() {
  }

}
