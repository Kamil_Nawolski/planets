import { Component, OnInit } from '@angular/core';
import { PlanetsService } from '../../services/planets.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Planet } from 'src/app/models/Planet';
import { DetailsService } from 'src/app/services/details.service';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { People } from 'src/app/models/People';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-planet-details',
	templateUrl: './planet-details.component.html',
	styleUrls: ['./planet-details.component.sass']
})
export class PlanetDetailsComponent implements OnInit {

	planet: Planet;
	residents: People[];
	isLoading: Boolean = true;

	constructor(private _planetsService: PlanetsService,
				private _detailsService: DetailsService,
				private _activatedRoute: ActivatedRoute,
				private _location: Location) { }

	ngOnInit() {
		let id = this._activatedRoute.snapshot.params['id'];
		this.getPlanetDetails(id);
	}

	setCurrentPlanetDetailsToSideBar() {
		this._detailsService.setPlanetDetails(this.planet);
	}

	goBack() {
		this._location.back();
	}

	getPlanetDetails(id: number) {
		this._planetsService.getPlanetDetails(id).subscribe(details => {
			this.planet = details;
			this.setCurrentPlanetDetailsToSideBar();
		},
		(err: HttpErrorResponse) => console.log(`nie udało się pobrać danych: błąd ${err.status}`),
		() => this.isLoading = false
		)
	}
}
