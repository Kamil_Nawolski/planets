import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { People } from 'src/app/models/People';
import { PlanetsService } from 'src/app/services/planets.service';
import { Planet } from 'src/app/models/Planet';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-residents-info',
	templateUrl: './residents-info.component.html',
	styleUrls: ['./residents-info.component.sass']
})
export class ResidentsInfoComponent implements OnInit {

	dataSource = new MatTableDataSource<People[]>();
	displayedColumns: string[] = ['name', 'height', 'mass', 'hair_color', 'skin_color'];
	isLoading: Boolean = true;

	@Input() planet: Planet;

	constructor(private _planetsService: PlanetsService) { }

	ngOnInit() {
		this.getResidents(this.planet);
	}

	setUnknownClass(element) {
		return { 'unknown': element === 'unknown' }
	}

	hideUnit(element): boolean {
		return element !== 'unknown';
	}

	getIdPeople(url: string) {
		let id = /\d+/g;
		return id.exec(url);
	}

	getResidents(planet: Planet) {
		this.isLoading = true;
		let residents = planet.residents;
		let peoples = [];
		residents.forEach(element => {
			let ids = this.getIdPeople(element);
			of(ids['0']).pipe(
				mergeMap(x => this._planetsService.getResidentals(x))
			).subscribe(i => {
				peoples.push(i);
				this.dataSource = new MatTableDataSource<People[]>(peoples);
			},
				(err: HttpErrorResponse) => console.log(`nie udało się pobrać danych: błąd ${err.status}`),
				() => this.isLoading = false
			);
		});
	}


}
