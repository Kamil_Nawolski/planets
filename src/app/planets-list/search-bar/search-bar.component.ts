import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.sass']
})
export class SearchBarComponent implements OnInit {

  @Output() eventSearch = new EventEmitter();

  searchPlanet: string = '';

  constructor() { }

  ngOnInit() {
  }

  findPlanet() {
    this.eventSearch.emit(this.searchPlanet);
  }
}
