import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { PlanetsService } from '../services/planets.service';
import { MatTableDataSource } from '@angular/material/table';
import { HttpErrorResponse } from '@angular/common/http';
import { Planet } from '../models/Planet';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { DetailsService } from '../services/details.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { ParamsPlanet } from '../models/ParamsPlanets';

@Component({
	selector: 'app-planets-list',
	templateUrl: './planets-list.component.html',
	styleUrls: ['./planets-list.component.sass']
})
export class PlanetsListComponent implements OnInit, OnDestroy {

	private _destroyed$ = new Subject();

	@ViewChild(MatPaginator) paginator: MatPaginator;
	displayedColumns: string[] = ['name', 'population', 'rotation_period', 'orbital_period', 'Link'];
	pageSizeOptions: number[] = [5, 10, 25, 100];
	dataSource = new MatTableDataSource<Planet>();
	pageEvent: PageEvent;
	totalCountPlanets: number;
	pageSize: number = 10;

	searchValue: string = '';
	pageIndex: number = 1;
	planets: Planet[] = [];
	selected: Planet;
	isLoading: Boolean = true;

	constructor(private _planetsService: PlanetsService,
		private _detailsService: DetailsService,
		private _router: Router) { }

	ngOnInit() {
		this.getPlanets(this.searchValue);
		this.dataSource.paginator = this.paginator;
		this.clearSideBar();
	}

	clearSideBar() {
		this._detailsService.setPlanetDetails(null);
	}

	ngOnDestroy() {
		this._destroyed$.next();
		this._destroyed$.complete();
	}

	setUnknownClass(element) {
		return { 'unknown': element === 'unknown' }
	}

	hideUnit(element): boolean {
		return element !== 'unknown';
	}

	getPlanetId(url: string) {
		let regexp = /\d+/g;
		return regexp.exec(url);
	}

	selectedRow(planet: Planet) {
		this.selected = planet;
	}

	getDetails(planet: Planet) {
		this._detailsService.setPlanetDetails(planet);
		this.selectedRow(planet);
	}

	goToDetails(planet: Planet) {
		let id = this.getPlanetId(planet.url);
		this._router.navigate([`/planets/${id}`]);
	}

	getServerData(page) {
		this.pageIndex = page.pageIndex + 1;
		this.getPlanets(this.searchValue);
	}

	searchPlanet(planet) {
		this.searchValue = planet;
		this.getPlanets(planet);
	}

	getPlanets(name: string) {
		this.isLoading = true;
		let params: ParamsPlanet = { serach: name, page: this.pageIndex };
		this._planetsService.getPlanets(params)
			.pipe(
				debounceTime(500),
				takeUntil(this._destroyed$)
			)
			.subscribe(planets => {
				this.dataSource = new MatTableDataSource(planets.results);
				this.totalCountPlanets = planets.count;
			},
				(err: HttpErrorResponse) => console.log(`nie udało się pobrać danych: błąd ${err.status}`),
				() => this.isLoading = false)
	};
}
