import { DetailsService } from '../services/details.service';
import { Component, OnInit } from '@angular/core';
import { Planet } from '../models/Planet';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.sass']
})
export class SideBarComponent implements OnInit {

  planetDetails: Planet;

  constructor(private _detailsService: DetailsService) { }
  
  ngOnInit() {
    this.getPlanetDetails();
  }

  getPlanetDetails() {
    this._detailsService.getPlanetDetails().subscribe(planet => this.planetDetails = planet);
  }
}
