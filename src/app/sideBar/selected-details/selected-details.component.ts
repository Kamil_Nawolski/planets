import { Component, OnInit } from '@angular/core';
import { DetailsService } from 'src/app/services/details.service';
import { Planet } from 'src/app/models/Planet';

@Component({
  selector: 'app-selected-details',
  templateUrl: './selected-details.component.html',
  styleUrls: ['./selected-details.component.sass']
})
export class SelectedDetailsComponent implements OnInit {

  planetDetails: Planet;

  constructor(private _detailsService: DetailsService) { }
  
  ngOnInit() {
    this.getPlanetDetails();
  }

  getPlanetDetails() {
    this._detailsService.getPlanetDetails().subscribe(planet => this.planetDetails = planet);
  }

}
