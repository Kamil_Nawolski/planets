import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MatSidenavModule, MatTableModule, MatInputModule, MatFormFieldModule, MatPaginatorModule, MatIconModule, MatProgressSpinnerModule, MatSortModule, MatButtonModule, MatTooltipModule, MatListModule, MatTabsModule, MatGridListModule,MatToolbarModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideBarComponent } from './sideBar/side-bar.component';
import { PlanetsListComponent } from './planets-list/planets-list.component';
import { SearchBarComponent } from './planets-list/search-bar/search-bar.component';
import { PlanetDetailsComponent } from './planets-list/planet-details/planet-details.component';
import { SelectedDetailsComponent } from './sideBar/selected-details/selected-details.component';
import { GeneralInfoComponent } from './planets-list/planet-details/general-info/general-info.component';
import { ResidentsInfoComponent } from './planets-list/planet-details/residents-info/residents-info.component';
import { FilmsInfoComponent } from './planets-list/planet-details/films-info/films-info.component';

const Material = [
  MatSidenavModule,
  MatTableModule,
  MatInputModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatButtonModule,
  MatTooltipModule,
  MatListModule,
  MatTabsModule,
  MatGridListModule,MatToolbarModule
];

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    PlanetsListComponent,
    SearchBarComponent,
    PlanetDetailsComponent,
    SelectedDetailsComponent,
    GeneralInfoComponent,
    ResidentsInfoComponent,
    FilmsInfoComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    Material
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
