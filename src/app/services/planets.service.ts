import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataTable } from '../models/DataTable';
import { PlanetDetails } from '../models/PlanetDetails';
import { ParamsPlanet } from '../models/ParamsPlanets';
import { People } from '../models/People';
import { Film } from '../models/Film';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  private _url:string = `https://swapi.co/api`;

  constructor(private http: HttpClient) { }

  getPlanets(params?: ParamsPlanet): Observable<DataTable> {
    return this.http.get<DataTable>(`${this._url}/planets`, {
      params: {
        search: params.serach,
        page: params.page.toString()
      }
    });
  }

  getPlanetDetails(id:number): Observable<PlanetDetails> {
    return this.http.get<PlanetDetails>(`${this._url}/planets/${id}`);
  }

  getResidentals(id: number): Observable<People> {
    return this.http.get<People>(`${this._url}/people/${id}`);
  }

  getFilmsDetails(id: number): Observable<Film>  {
      return this.http.get<Film>(`${this._url}/films/${id}`);
  }
}
