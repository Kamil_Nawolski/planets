import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Planet } from '../models/Planet';

@Injectable({
  providedIn: 'root'
})
export class DetailsService {

  private _planetDetails = new Subject<Planet>();

  constructor() { }

  setPlanetDetails(planet: Planet): void {
    this._planetDetails.next(planet);
  }

  getPlanetDetails(): Observable<Planet> {
    return this._planetDetails.asObservable();
  }
}
