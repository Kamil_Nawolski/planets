import { Planet } from './Planet';

export interface DataTable {
    count: number;
    next?: string;
    previous?: string;
    results: Planet[];
    
}